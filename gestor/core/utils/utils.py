import hashlib


def gen_sha1( cadena ):
    
    hash_object = hashlib.sha1( cadena )
    
    hex_dig = hash_object.hexdigest()
    
    return hex_dig
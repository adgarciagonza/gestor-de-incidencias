import datetime, random
from mongoengine import Q

from flask import redirect, url_for, session



 
# import yescity.core.models.Geolocation as Geolocation
# import yescity.core.models.Relation as Relation
# import yescity.core.models.Message as Message
# import yescity.core.models.Email as Email
# 
# import yescity.core.utils.Utils as Utils

import gestor.core.utils.utils as Utils

gender = {
          "male": True,
          "female": False
          }


from gestor import db

class User(db.Document):
    
    #Comprobar que no se puedan duplicar los usernames
    
    created_at = db.DateTimeField(default=datetime.datetime.now, required=True)
    
    first_name = db.StringField(max_length=255, required=True)
    last_name = db.StringField(max_length=255, required=True)
    
    username = db.StringField(max_length=255, unique=True)
    password = db.StringField(max_length=255)
    
    email = db.EmailField(max_length=255, required=True)
    
    administrator = db.BooleanField(default= False);
    birth_date = db.DateTimeField(required=True)
    gender = db.BooleanField(default = gender["male"])
    section = db.IntField()
    username = db.StringField(max_length=255)
    administrator = db.BooleanField(default = False)
    address = db.StringField(max_length=255)
    profile_image = db.StringField(max_length=255)
    
    
    meta = {
        'allow_inheritance': True,
        'indexes': ['-created_at',],
        'ordering': ['-created_at']
    }
    
    def get_profile_image_route(self):
        
        if self.profile_image_filename:
            
            return '/static/upload/images/' + self.profile_image_filename
        
        return '/static/images/no_profile_image.png'
    
    def get_full_name(self):
        
        return self.first_name + ' ' + self.last_name
    
    def check_password(self,password):
        
        if Utils.gen_sha1(password) == self.password:
            return True
        else:
            return False
        

def create(first_name, last_name, email, administrator,
           birth_date, address  ):
    
    username = first_name + '.' +  last_name    
    
    counter = 1;
    
    #Comprobamos que el username es unico
    while True:
        
        user_repetido = get_user(username)
        
        if user_repetido == None:
            break;
        
        else:
            if counter > 1:
                username = username[0: len(username) - len(str(counter-1)) ]
                
            username = username + str(counter)
            counter += 1            
        
            
    password = ""
    
    password_hash = Utils.gen_sha1(password)
    
    user = User( first_name = first_name, last_name = last_name, username = username, password = password_hash,
                 email = email, administrator = administrator, address = address, profile_image ="",
                 birth_date = birth_date )
    
    user.save()
    
    return user
    
    
def get_user(username):
    try:
        user = User.objects.filter( username__iexact=username )[0]
    except:
        return None
    else:
        return user
    
    


    
from gestor import db

from mongoengine import Q
import datetime

status = {
          0: "sin asignar",
          1: "asignada",
          2: "pendiente",
          3: ""
          }




class Incident(db.Document):
    
    id = db.StringField(max_length=255, required=True, unique=True)
    created_at = db.DateTimeField(default=datetime.datetime.now, required=True)
    title = db.StringField(max_length=255, required=True)
    description = db.StringField()
    assigned_to = db.ReferenceField(Usuario)
    status = db.IntField()
    department = db.StringField(max_length=255, required=True)
    CoordX = db.FloatField()
    CoordY = db.FloatField()
    urgency = db.IntField()
    
    

def create(nombre_usuario, password, departamento):    
    
    id = Incidencia.objects.count() + 1
    
    id = id.zfill(10)
    
    incidencia = Usuario(nombre_usuario = nombre_usuario, password = password, admin = False, departamento = departamento ) 
    
    usuario.save()


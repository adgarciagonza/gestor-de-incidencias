from flask import Flask
from flask.ext.mongoengine import MongoEngine
from flask.ext.script import Manager

import config.Config as Config

app = Flask(__name__)
app.config["MONGODB_SETTINGS"] = Config.MONGODB_SETTINGS
app.secret_key = Config.SECRET_KEY 
app.debug = Config.DEBUG

db = MongoEngine(app)

manager = Manager(app)

#===============================================================================
# 
# # IMPORTAR BLUEPRINTS MIGUEL
# import core.login.app.yescity_login as yescity_login
# import core.signup.app.yescity_signup as yescity_signup
# import core.signup.app.remind_password as yescity_remind_password
# import core.api.app.yescity_api as yescity_api
# import core.signup.app.signup_facebook as yescity_facebook
# import core.user.app.yescity_user as yescity_user
# import core.post.app.yescity_post as yescity_post
# import core.comment.app.yescity_comment as yescity_comment
#   
#   
# # IMPORTAR BLUEPRINTS ADRIAN
# import core.relations.app.yescity_relations as yescity_relations
# import core.messages.app.yescity_messages as yescity_messages
# import core.chat.app.yescity_chat as yescity_chat
# import core.geolocation.app.yescity_geolocation as yescity_geolocation
# #import core.geolocation.app.yescity_load_opendata as yescity_load_opendata
# import apps.agencia_asturias.app.agencia_asturias as agencia_asturias
# import apps.telecable.app.telecable as telecable
#  
#   
# #Registrar Blueprints Miguel
# app.register_blueprint(yescity_login.yescity_login)
# app.register_blueprint(yescity_signup.yescity_signup)
# app.register_blueprint(yescity_remind_password.yescity_remind_password)
# app.register_blueprint(yescity_api.yescity_api)
# app.register_blueprint(yescity_facebook.yescity_facebook)
# app.register_blueprint(yescity_user.yescity_user)
# app.register_blueprint(yescity_post.yescity_post)
# app.register_blueprint(yescity_comment.yescity_comment)
# 
# #Registrar Blueprints Adrian 
# app.register_blueprint(yescity_chat.yescity_chat)
# app.register_blueprint(yescity_relations.yescity_relations)
# app.register_blueprint(yescity_messages.yescity_messages)
# app.register_blueprint(yescity_geolocation.yescity_geolocation)
# #app.register_blueprint(yescity_load_opendata.yescity_load_opendata)
# app.register_blueprint(agencia_asturias.agencia_asturias)
# app.register_blueprint(telecable.telecable)
#===============================================================================

import gestor.index

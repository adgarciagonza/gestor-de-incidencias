# Set the path
import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

# Python and Flask extensions

from flask import Flask, Blueprint, url_for, render_template, request, jsonify
from flask import make_response, abort, session, redirect, g

import datetime


from gestor import app, db

import core.models.User as User

#===============================================================================
# from yescity import yescity_login
# import yescity.core.utils.Utils as Utils
# import yescity.core.models.Twitter as Twitter
# import yescity.core.models.Update as Update
#===============================================================================

# Config
import config.Config as Config

# Models
#===============================================================================
# import core.models.User as User
# import core.models.Post as Post
# import yescity.core.models.Comment as Comment
# import yescity.core.models.Mention as Mention
#===============================================================================

@app.route('/', methods=['POST', 'GET'])
def index():
    
    
    if request.method == 'POST':
        
        username = request.form['username']
        
        password = request.form['password']
        
        user = User.get_user(username)
        
        if user != None:
        
            if user.check_password(password) == True:
                
                session['user'] = user
                
                return render_template('index.html', error = 0)
            
                
        return render_template('login.html', error = 1)
        
    else:
            
        return render_template('login.html', error = 0)
    


@app.route('/quienes-somos/')
def quienes_somos():
    return 'Descripci&oacute;n del proyecto'


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html', inicio=url_for('index')), 404

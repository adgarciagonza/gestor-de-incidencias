from gestor import app, db

import config.Config as Config

if __name__ == '__main__':
    app.run( host=Config.HOST, port = Config.PORT)
